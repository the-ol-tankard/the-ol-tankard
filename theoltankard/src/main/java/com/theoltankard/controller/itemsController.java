package com.theoltankard.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.theoltankard.dto.items;
import com.theoltankard.service.itemsImpl;

@RestController
@CrossOrigin(origins = "*", methods= {RequestMethod.GET,RequestMethod.POST,RequestMethod.PUT,RequestMethod.DELETE})
@RequestMapping("/api")
public class itemsController {
	@Autowired
	itemsImpl itemsImpl;
	
	//Listar todos
	@GetMapping("/items")
	public List<items> listaritems(){
		return itemsImpl.listarItems();
	}
	
	//Buscar por id
	@GetMapping("/items/{id}")
	public items itemsXID(@PathVariable(name="id") int id) {
		
		items items_xid= new items();
		
		items_xid=itemsImpl.itemsXID(id);
		
		System.out.println("items XID: "+ items_xid);
		
		return items_xid;
	}
}

