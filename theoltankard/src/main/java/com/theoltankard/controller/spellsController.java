package com.theoltankard.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.theoltankard.dto.spells;
import com.theoltankard.service.spellsImpl;

@RestController
@CrossOrigin(origins = "*", methods= {RequestMethod.GET,RequestMethod.POST,RequestMethod.PUT,RequestMethod.DELETE})
@RequestMapping("/api")
public class spellsController {
	@Autowired
	spellsImpl spellsImpl;
	
	//Listar todos
	@GetMapping("/spells")
	public List<spells> listarspells(){
		return spellsImpl.listarSpells();
	}
	
	//Buscar por id
	@GetMapping("/spells/{id}")
	public spells spellsXID(@PathVariable(name="id") int id) {
		
		spells spells_xid= new spells();
		
		spells_xid=spellsImpl.spellsXID(id);
		
		System.out.println("spells XID: "+ spells_xid);
		
		return spells_xid;
	}
}

