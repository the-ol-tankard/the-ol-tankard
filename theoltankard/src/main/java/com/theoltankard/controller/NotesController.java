package com.theoltankard.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.theoltankard.dto.Notes;
import com.theoltankard.service.NotesServiceImpl;

@RestController
@CrossOrigin(origins = "*", methods= {RequestMethod.GET,RequestMethod.POST,RequestMethod.PUT,RequestMethod.DELETE})
@RequestMapping("/api")
public class NotesController {
	
	@Autowired
	NotesServiceImpl noteServiceImpl;
	
	@GetMapping("/notes")
	public List<Notes> listNotes() {
		
		return noteServiceImpl.listNotes();
		
	}
	
	@PostMapping("/notes")
	public Notes saveNote(@RequestBody Notes note) {
		
		return noteServiceImpl.saveNote(note);
		
	}
	
	@GetMapping("/notes/{id}")
	public Notes noteXID(@PathVariable(name="id") int id) {
		
		Notes Note_xid = new Notes();
		
		Note_xid = noteServiceImpl.noteXID(id);
		
		System.out.println("Note XID: " + Note_xid);
		
		return Note_xid;
		
	}
	
	@PutMapping("/notes/{id}")
	public Notes updateNote(@PathVariable(name = "id") int id, @RequestBody Notes note) {
		
		Notes Note_selected = new Notes();
		Notes Note_updated = new Notes();
		
		Note_selected = noteServiceImpl.noteXID(id);
		
		Note_selected.setName(note.getName());
		Note_selected.setText(note.getText());
		Note_selected.setDate(note.getDate());
		
		Note_updated = noteServiceImpl.updateNote(Note_selected);
		
		System.out.println("The updated note is: " + Note_updated);
		
		return Note_updated;
		
	}
	
	@DeleteMapping("/notes/{id}")
	public void deleteNote(@PathVariable(name = "id") int id) {
		
		noteServiceImpl.deleteNote(id);
		
	}
	
}
