package com.theoltankard.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.theoltankard.dto.notesMaster;
import com.theoltankard.service.notesMasterImpl;

@RestController
@CrossOrigin(origins = "*", methods= {RequestMethod.GET,RequestMethod.POST,RequestMethod.PUT,RequestMethod.DELETE})
@RequestMapping("/api")
public class notesMasterController {
	
	@Autowired
	notesMasterImpl notesMastererviceImpl;
	
	@GetMapping("/notesMaster")
	public List<notesMaster> listnotesMaster() {
		
		return notesMastererviceImpl.listNotesMaster();
		
	}
	
	@PostMapping("/notesMaster")
	public notesMaster saveNote(@RequestBody notesMaster note) {
		
		return notesMastererviceImpl.saveNotesMaster(note);
		
	}
	
	@GetMapping("/notesMaster/{id}")
	public notesMaster noteXID(@PathVariable(name="id") int id) {
		
		notesMaster Note_xid = new notesMaster();
		
		Note_xid = notesMastererviceImpl.notesMasterXID(id);
		
		System.out.println("Note XID: " + Note_xid);
		
		return Note_xid;
		
	}
	
	@PutMapping("/notesMaster/{id}")
	public notesMaster updateNote(@PathVariable(name = "id") int id, @RequestBody notesMaster note) {
		
		notesMaster Note_selected = new notesMaster();
		notesMaster Note_updated = new notesMaster();
		
		Note_selected = notesMastererviceImpl.notesMasterXID(id);
		
		Note_selected.setName(note.getName());
		Note_selected.setText(note.getText());
		Note_selected.setDate(note.getDate());
		
		Note_updated = notesMastererviceImpl.updateNotesMaster(Note_selected);
		
		System.out.println("The updated note is: " + Note_updated);
		
		return Note_updated;
		
	}
	
	@DeleteMapping("/notesMaster/{id}")
	public void deleteNote(@PathVariable(name = "id") int id) {
		
		notesMastererviceImpl.deleteNotesMaster(id);
		
	}
	
}

