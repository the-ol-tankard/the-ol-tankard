package com.theoltankard.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.theoltankard.dto.Images;
import com.theoltankard.service.ImagesServiceImpl;

@RestController
@CrossOrigin(origins = "*", methods= {RequestMethod.GET,RequestMethod.POST,RequestMethod.PUT,RequestMethod.DELETE})
@RequestMapping("/api")
public class ImagesController {
	
	@Autowired
	ImagesServiceImpl imageServiceImpl;
	
	@GetMapping("/images")
	public List<Images> listImages() {
		
		return imageServiceImpl.listImages();
		
	}
	
	@PostMapping("/images")
	public Images saveImage(@RequestBody Images image) {
		
		return imageServiceImpl.saveImage(image);
		
	}
	
	@GetMapping("/images/{id}")
	public Images imageXID(@PathVariable(name="id") int id) {
		
		Images Image_xid = new Images();
		
		Image_xid = imageServiceImpl.imageXID(id);
		
		System.out.println("Image XID: " + Image_xid);
		
		return Image_xid;
		
	}
	
	@PutMapping("/images/{id}")
	public Images updateImage(@PathVariable(name = "id") int id, @RequestBody Images image) {
		
		Images Image_selected = new Images();
		Images Image_updated = new Images();
		
		Image_selected = imageServiceImpl.imageXID(id);
		
		Image_selected.setName(image.getName());
		
		Image_updated = imageServiceImpl.updateImage(Image_selected);
		
		System.out.println("The updated image is: " + Image_updated);
		
		return Image_updated;
		
	}
	
	@DeleteMapping("/images/{id}")
	public void deleteImage(@PathVariable(name = "id") int id) {
		
		imageServiceImpl.deleteImage(id);
		
	}
	
}
