package com.theoltankard.controller;


import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.theoltankard.dto.Races;
import com.theoltankard.service.RacesServiceImpl;

@RestController
@CrossOrigin(origins = "*", methods= {RequestMethod.GET,RequestMethod.POST,RequestMethod.PUT,RequestMethod.DELETE})
@RequestMapping("/api")
public class RacesController {
	
	@Autowired
	RacesServiceImpl raceServiceImpl;
	
	@GetMapping("/races")
	public List<Races> listRaces() {
		
		return raceServiceImpl.listRaces();
		
	}
	
	@PostMapping("/races")
	public Races saveRace(@RequestBody Races race) {
		
		return raceServiceImpl.saveRace(race);
		
	}
	
	@GetMapping("/races/{id}")
	public Races raceXID(@PathVariable(name="id") int id) {
		
		Races Race_xid = new Races();
		
		Race_xid = raceServiceImpl.raceXID(id);
		
		System.out.println("Race XID: " + Race_xid);
		
		return Race_xid;
		
	}
	
	@PutMapping("/races/{id}")
	public Races updateRace(@PathVariable(name = "id") int id, @RequestBody Races race) {
		
		Races Race_selected = new Races();
		Races Race_updated = new Races();
		
		Race_selected = raceServiceImpl.raceXID(id);
		
		Race_selected.setName(race.getName());
		Race_selected.setSpeed(race.getSpeed());
		Race_selected.setDescription(race.getDescription());
		Race_selected.setAbilityScore(race.getAbilityScore());
		Race_selected.setTrait1(race.getTrait1());
		Race_selected.setTrait2(race.getTrait2());
		Race_selected.setTrait3(race.getTrait3());
		
		Race_updated = raceServiceImpl.updateRace(Race_selected);
		
		System.out.println("The updated race is: " + Race_updated);
		
		return Race_updated;
		
	}
	
	@DeleteMapping("/races/{id}")
	public void deleteRace(@PathVariable(name = "id") int id) {
		
		raceServiceImpl.deleteRace(id);
		
	}
	
}