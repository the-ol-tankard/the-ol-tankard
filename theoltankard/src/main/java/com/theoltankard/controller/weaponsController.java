package com.theoltankard.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.theoltankard.dto.weapons;
import com.theoltankard.service.weaponsImpl;

@RestController
@CrossOrigin(origins = "*", methods= {RequestMethod.GET,RequestMethod.POST,RequestMethod.PUT,RequestMethod.DELETE})
@RequestMapping("/api")
public class weaponsController {
	@Autowired
	weaponsImpl weaponsImpl;
	
	//Listar todos
	@GetMapping("/weapons")
	public List<weapons> listarweapons(){
		return weaponsImpl.listarWeapons();
	}
	
	//Buscar por id
	@GetMapping("/weapons/{id}")
	public weapons weaponsXID(@PathVariable(name="id") int id) {
		
		weapons weapons_xid= new weapons();
		
		weapons_xid=weaponsImpl.weaponsXID(id);
		
		System.out.println("weapons XID: "+ weapons_xid);
		
		return weapons_xid;
	}
}

