package com.theoltankard.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.theoltankard.dto.clases;
import com.theoltankard.service.clasesImpl;

@RestController
@CrossOrigin(origins = "*", methods= {RequestMethod.GET,RequestMethod.POST,RequestMethod.PUT,RequestMethod.DELETE})
@RequestMapping("/api")
public class clasesController {
	@Autowired
	clasesImpl clasesImpl;
	
	//Listar todos
	@GetMapping("/clases")
	public List<clases> listarclases(){
		return clasesImpl.listarClases();
	}
	
	//Buscar por id
	@GetMapping("/clases/{id}")
	public clases clasesXID(@PathVariable(name="id") int id) {
		
		clases clases_xid= new clases();
		
		clases_xid=clasesImpl.clasesXID(id);
		
		System.out.println("clases XID: "+ clases_xid);
		
		return clases_xid;
	}
}

