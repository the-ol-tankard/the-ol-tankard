package com.theoltankard.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.theoltankard.dto.skills;
import com.theoltankard.service.skillslmpl;

@RestController
@CrossOrigin(origins = "*", methods= {RequestMethod.GET,RequestMethod.POST,RequestMethod.PUT,RequestMethod.DELETE})
@RequestMapping("/api")
public class skillsController {
	
	@Autowired
	skillslmpl skillslmpl;
	
	@GetMapping("/skills")
	public List<skills> listarskills(){
		return skillslmpl.listarskills();
	}
	
	@PostMapping("/skills")
	public skills salvarskills(@RequestBody skills skills) {
		
		return skillslmpl.guardarskills(skills);
	}
	
	@GetMapping("/skills/{id}")
	public skills skillsXID(@PathVariable(name="id") int id) {
		
		skills skills_xid= new skills();
		
		skills_xid=skillslmpl.skillsXID(id);
		
		System.out.println("skills XID: "+skills_xid);
		
		return skills_xid;
	}

}
