package com.theoltankard.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.theoltankard.dto.primary_Stats;
import com.theoltankard.service.primary_Statslmpl;


@RestController
@CrossOrigin(origins = "*", methods= {RequestMethod.GET,RequestMethod.POST,RequestMethod.PUT,RequestMethod.DELETE})
@RequestMapping("/api")
public class primary_StatsController {
	
	@Autowired
	primary_Statslmpl primary_Statslmpl;
	
	@GetMapping("/primary_Stats")
	public List<primary_Stats> listarprimary_Stats(){
		return primary_Statslmpl.listarprimary_Stats();
	}
	
	@PostMapping("/primary_Stats")
	public primary_Stats salvarprimary_Stats(@RequestBody primary_Stats primary_Stats) {
		
		return primary_Statslmpl.guardarprimary_Stats(primary_Stats);
	}
	
	@GetMapping("/primary_Stats/{id}")
	public primary_Stats primary_StatsXID(@PathVariable(name="id") int id) {
		
		primary_Stats primary_Stats_xid= new primary_Stats();
		
		primary_Stats_xid=primary_Statslmpl.primary_StatsXID(id);
		
		System.out.println("primary_Stats XID: "+primary_Stats_xid);
		
		return primary_Stats_xid;
	}

}
