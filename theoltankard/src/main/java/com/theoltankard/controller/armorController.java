package com.theoltankard.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.theoltankard.dto.armor;
import com.theoltankard.service.armorImpl;

@RestController
@CrossOrigin(origins = "*", methods= {RequestMethod.GET,RequestMethod.POST,RequestMethod.PUT,RequestMethod.DELETE})
@RequestMapping("/api")
public class armorController {
	@Autowired
	armorImpl armorImpl;
	
	//Listar todos
	@GetMapping("/armor")
	public List<armor> listararmor(){
		return armorImpl.listarArmor();
	}
	
	//Buscar por id
	@GetMapping("/armor/{id}")
	public armor armorXID(@PathVariable(name="id") int id) {
		
		armor armor_xid= new armor();
		
		armor_xid=armorImpl.armorXID(id);
		
		System.out.println("armor XID: "+ armor_xid);
		
		return armor_xid;
	}
}

