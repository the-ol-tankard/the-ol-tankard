package com.theoltankard.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.theoltankard.dto.cantrip;
import com.theoltankard.service.cantripImpl;

@RestController
@CrossOrigin(origins = "*", methods= {RequestMethod.GET,RequestMethod.POST,RequestMethod.PUT,RequestMethod.DELETE})
@RequestMapping("/api")
public class cantripController {
	@Autowired
	cantripImpl cantripImpl;
	
	//Listar todos
	@GetMapping("/cantrip")
	public List<cantrip> listarCantrip(){
		return cantripImpl.listarCantrip();
	}
	
	//Buscar por id
	@GetMapping("/cantrip/{id}")
	public cantrip cantripXID(@PathVariable(name="id") int id) {
		
		cantrip cantrip_xid= new cantrip();
		
		cantrip_xid=cantripImpl.cantripXID(id);
		
		System.out.println("cantrip XID: "+ cantrip_xid);
		
		return cantrip_xid;
	}
}

