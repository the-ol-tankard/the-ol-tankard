package com.theoltankard.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.theoltankard.dto.characteroltankard;
import com.theoltankard.service.characteroltankardlmpl;

@RestController
@CrossOrigin(origins = "*", methods= {RequestMethod.GET,RequestMethod.POST,RequestMethod.PUT,RequestMethod.DELETE})
@RequestMapping("/api")
public class characteroltankardController {
	
	@Autowired
	characteroltankardlmpl characteroltankardlmpl;
	
	@GetMapping("/characteroltankard")
	public List<characteroltankard> listarcharacteroltankard(){
		return characteroltankardlmpl.listarcharacteroltankard();
	}
	
	@PostMapping("/characteroltankard")
	public characteroltankard salvarcharacteroltankard(@RequestBody characteroltankard characteroltankard) {
		
		return characteroltankardlmpl.guardarcharacteroltankard(characteroltankard);
	}
	
	@GetMapping("/characteroltankard/{id}")
	public characteroltankard characteroltankardXID(@PathVariable(name="id") int id) {
		
		characteroltankard characteroltankard_xid= new characteroltankard();
		
		characteroltankard_xid=characteroltankardlmpl.characteroltankardXID(id);
		
		System.out.println("characteroltankard XID: "+characteroltankard_xid);
		
		return characteroltankard_xid;
	}

	@PutMapping("/characteroltankard/{id}")
	public characteroltankard actualizarcharacteroltankard(@PathVariable(name="id")int id,@RequestBody characteroltankard characteroltankard) {
		
		characteroltankard characteroltankard_seleccionado= new characteroltankard();
		characteroltankard characteroltankard_actualizado= new characteroltankard();
		
		characteroltankard_seleccionado= characteroltankardlmpl.characteroltankardXID(id);
		
		characteroltankard_seleccionado.setName(characteroltankard.getName());
		characteroltankard_seleccionado.setHp(characteroltankard.getHp());
		characteroltankard_seleccionado.setClasses(characteroltankard.getClasses());
		characteroltankard_seleccionado.setSubclassid(characteroltankard.getSubclassid());
		characteroltankard_seleccionado.setRace(characteroltankard.getRace());
		characteroltankard_seleccionado.setAlignment(characteroltankard.getAlignment());
		characteroltankard_seleccionado.setBackground(characteroltankard.getBackground());
		characteroltankard_seleccionado.setAge(characteroltankard.getAge());
		characteroltankard_seleccionado.setSize(characteroltankard.getSize());
		characteroltankard_seleccionado.setPrimaryCharacter(characteroltankard.getPrimaryCharacter());
		characteroltankard_seleccionado.setSkillsCharacter(characteroltankard.getSkillsCharacter());
		
		characteroltankard_actualizado = characteroltankardlmpl.actualizarcharacteroltankard(characteroltankard_seleccionado);
		
		System.out.println("El cajero actualizado es: "+ characteroltankard_actualizado);
		
		return characteroltankard_actualizado;
	}

	@DeleteMapping("/characteroltankard/{id}")
	public void eliminarcharacteroltankard(@PathVariable(name="id")int id) {
		characteroltankardlmpl.eliminarcharacteroltankard(id);
	}

}
