package com.theoltankard.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.theoltankard.dto.subclass;
import com.theoltankard.service.subclassImpl;

@RestController
@CrossOrigin(origins = "*", methods= {RequestMethod.GET,RequestMethod.POST,RequestMethod.PUT,RequestMethod.DELETE})
@RequestMapping("/api")
public class subclassController {
	@Autowired
	subclassImpl subclassImpl;
	
	//Listar todos
	@GetMapping("/subclass")
	public List<subclass> listarsubclass(){
		return subclassImpl.listarSubclass();
	}
	
	//Buscar por id
	@GetMapping("/subclass/{id}")
	public subclass subclassXID(@PathVariable(name="id") int id) {
		
		subclass subclass_xid= new subclass();
		
		subclass_xid=subclassImpl.subclassXID(id);
		
		System.out.println("subclass XID: "+ subclass_xid);
		
		return subclass_xid;
	}
}

