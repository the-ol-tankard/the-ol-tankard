package com.theoltankard.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.bind.annotation.RequestMethod;


import com.theoltankard.dto.Monsters;
import com.theoltankard.service.MonstersServiceImpl;

@RestController
@CrossOrigin(origins = "*", methods= {RequestMethod.GET,RequestMethod.POST,RequestMethod.PUT,RequestMethod.DELETE})
@RequestMapping("/api")
public class MonstersController {
	
	@Autowired
	MonstersServiceImpl monsterServiceImpl;
	
	@GetMapping("/monsters")
	public List<Monsters> listMonsters() {
		
		return monsterServiceImpl.listMonsters();
		
	}
	
	@PostMapping("/monsters")
	public Monsters saveMonster(@RequestBody Monsters monster) {
		
		return monsterServiceImpl.saveMonster(monster);
		
	}
	
	@GetMapping("/monsters/{id}")
	public Monsters monsterXID(@PathVariable(name="id") int id) {
		
		Monsters Monster_xid = new Monsters();
		
		Monster_xid = monsterServiceImpl.monsterXID(id);
		
		System.out.println("Monster XID: " + Monster_xid);
		
		return Monster_xid;
		
	}
	
	@PutMapping("/monsters/{id}")
	public Monsters updateMonster(@PathVariable(name = "id") int id, @RequestBody Monsters monster) {
		
		Monsters Monster_selected = new Monsters();
		Monsters Monster_updated = new Monsters();
		
		Monster_selected = monsterServiceImpl.monsterXID(id);
		
		Monster_selected.setName(monster.getName());
		Monster_selected.setDescription(monster.getDescription());
		Monster_selected.setArmorClass(monster.getArmorClass());
		Monster_selected.setHitPoints(monster.getHitPoints());
		Monster_selected.setAttack(monster.getAttack());
		Monster_selected.setSpeed(monster.getSpeed());
		Monster_selected.setStr(monster.getStr());
		Monster_selected.setDex(monster.getDex());
		Monster_selected.setCon(monster.getCon());
		Monster_selected.setInte(monster.getInte());
		Monster_selected.setWis(monster.getWis());
		Monster_selected.setCha(monster.getCha());
		
		Monster_updated = monsterServiceImpl.updateMonster(Monster_selected);
		
		System.out.println("The updated monster is: " + Monster_updated);
		
		return Monster_updated;
		
	}
	
	@DeleteMapping("/monsters/{id}")
	public void deleteMonster(@PathVariable(name = "id") int id) {
		
		monsterServiceImpl.deleteMonster(id);
		
	}
	
}
