package com.theoltankard.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.theoltankard.dto.Alignment;
import com.theoltankard.service.AlignmentServiceImpl;

@RestController
@CrossOrigin(origins = "*", methods= {RequestMethod.GET,RequestMethod.POST,RequestMethod.PUT,RequestMethod.DELETE})
@RequestMapping("/api")
public class AlignmentController {
	
	@Autowired
	AlignmentServiceImpl alignmentServiceImpl;
	
	@GetMapping("/alignment")
	public List<Alignment> listAlignment() {
		
		return alignmentServiceImpl.listAlignment();
		
	}
	
	@PostMapping("/alignment")
	public Alignment saveAlignment(@RequestBody Alignment alignment) {
		
		return alignmentServiceImpl.saveAlignment(alignment);
		
	}
	
	@GetMapping("/alignment/{id}")
	public Alignment alignmentXID(@PathVariable(name="id") int id) {
		
		Alignment Alignment_xid = new Alignment();
		
		Alignment_xid = alignmentServiceImpl.alignmentXID(id);
		
		System.out.println("Alignment XID: " + Alignment_xid);
		
		return Alignment_xid;
		
	}
	
	@PutMapping("/alignment/{id}")
	public Alignment updateAlignment(@PathVariable(name = "id") int id, @RequestBody Alignment alignment) {
		
		Alignment Alignment_selected = new Alignment();
		Alignment Alignment_updated = new Alignment();
		
		Alignment_selected = alignmentServiceImpl.alignmentXID(id);
		
		Alignment_selected.setTipo(alignment.getTipo());
		
		Alignment_updated = alignmentServiceImpl.updateAlignment(Alignment_selected);
		
		System.out.println("The updated alignment is: " + Alignment_updated);
		
		return Alignment_updated;
		
	}
	
	@DeleteMapping("/alignment/{id}")
	public void deleteAlignment(@PathVariable(name = "id") int id) {
		
		alignmentServiceImpl.deleteAlignment(id);
		
	}
	
}
