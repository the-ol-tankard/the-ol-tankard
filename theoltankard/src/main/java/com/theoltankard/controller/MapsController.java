package com.theoltankard.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.theoltankard.dto.Maps;
import com.theoltankard.service.MapsServiceImpl;

@RestController
@CrossOrigin(origins = "*", methods= {RequestMethod.GET,RequestMethod.POST,RequestMethod.PUT,RequestMethod.DELETE})
@RequestMapping("/api")
public class MapsController {
	
	@Autowired
	MapsServiceImpl mapServiceImpl;
	
	@GetMapping("/maps")
	public List<Maps> listMaps() {
		
		return mapServiceImpl.listMaps();
		
	}
	
	@PostMapping("/maps")
	public Maps saveMap(@RequestBody Maps map) {
		
		return mapServiceImpl.saveMap(map);
		
	}
	
	@GetMapping("/maps/{id}")
	public Maps mapXID(@PathVariable(name="id") int id) {
		
		Maps Map_xid = new Maps();
		
		Map_xid = mapServiceImpl.mapXID(id);
		
		System.out.println("Map XID: " + Map_xid);
		
		return Map_xid;
		
	}
	
	@PutMapping("/maps/{id}")
	public Maps updateMap(@PathVariable(name = "id") int id, @RequestBody Maps map) {
		
		Maps Map_selected = new Maps();
		Maps Map_updated = new Maps();
		
		Map_selected = mapServiceImpl.mapXID(id);
		
		Map_selected.setName(map.getName());
		Map_selected.setContent(map.getContent());
		Map_selected.setDate(map.getDate());
		
		Map_updated = mapServiceImpl.updateMap(Map_selected);
		
		System.out.println("The updated map is: " + Map_updated);
		
		return Map_updated;
		
	}
	
	@DeleteMapping("/maps/{id}")
	public void deleteMap(@PathVariable(name = "id") int id) {
		
		mapServiceImpl.deleteMap(id);
		
	}
	
}
