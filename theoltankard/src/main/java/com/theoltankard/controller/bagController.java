package com.theoltankard.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.theoltankard.dto.bag;
import com.theoltankard.service.bagImpl;

@RestController
@CrossOrigin(origins = "*", methods= {RequestMethod.GET,RequestMethod.POST,RequestMethod.PUT,RequestMethod.DELETE})
@RequestMapping("/api")
public class bagController {
	@Autowired
	bagImpl bagImpl;
	
	@GetMapping("/bag")
	public List<bag> listarbag(){
		return bagImpl.listarbag();
	}
	
	@PostMapping("/bag")
	public bag guardarbag(@RequestBody bag bag) {
		
		return bagImpl.guardarBag(bag);
	}
	
	@GetMapping("/bag/{id}")
	public bag bagXID(@PathVariable(name="id") int id) {
		
		bag bag_xid= new bag();
		
		bag_xid=bagImpl.bagXID(id);
		
		System.out.println("bag XID: "+bag_xid);
		
		return bag_xid;
	}
	
	@PutMapping("/bag/{id}")
	public bag actualizarbag(@PathVariable(name="id")int id,@RequestBody bag bag) {
		
		bag bag_seleccionado= new bag();
		bag bag_actualizado= new bag();
		
		bag_seleccionado= bagImpl.bagXID(id);
		
		bag_seleccionado.setQuantity(bag.getQuantity());
		bag_seleccionado.setPlayer(bag.getPlayer());
		bag_seleccionado.setItems(bag.getItems());
		bag_seleccionado.setArmor(bag.getArmor());
		bag_seleccionado.setWeapons(bag.getWeapons());
	

		
		bag_actualizado = bagImpl.actualizarBag(bag_seleccionado);
		
		System.out.println("El bag actualizado es: "+ bag_actualizado);
		
		return bag_actualizado;
	}
	
	@DeleteMapping("/bag/{id}")
	public void eliminarbag(@PathVariable(name="id")int id) {
		bagImpl.eliminarbag(id);
	}
}

