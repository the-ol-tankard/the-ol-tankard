package com.theoltankard.service;

import java.util.List;

import com.theoltankard.dto.Races;

public interface IRacesService {

	public List<Races> listRaces();
	
	public Races saveRace(Races race);
	
	public Races raceXID(int id);
	
	public Races updateRace(Races race);
	
	public void deleteRace(int id);

}
