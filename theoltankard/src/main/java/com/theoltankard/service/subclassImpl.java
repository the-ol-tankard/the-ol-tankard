package com.theoltankard.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.theoltankard.dao.subclassDao;
import com.theoltankard.dto.subclass;

@Service
public class subclassImpl implements subclassService {
	@Autowired
	subclassDao subclassDao;
	
	@Override
	public List<subclass> listarSubclass() {
		return subclassDao.findAll();
	}

	@Override
	public subclass subclassXID(int id) {
		return subclassDao.findById(id).get();
	}
}

