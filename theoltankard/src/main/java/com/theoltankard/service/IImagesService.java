package com.theoltankard.service;

import java.util.List;

import com.theoltankard.dto.Images;

public interface IImagesService {

	public List<Images> listImages();
	
	public Images saveImage(Images image);
	
	public Images imageXID(int id);
	
	public Images updateImage(Images image);
	
	public void deleteImage(int id);

}
