package com.theoltankard.service;

import java.util.List;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.theoltankard.dao.notesMasterDao;
import com.theoltankard.dto.notesMaster;
@Service
public class notesMasterImpl implements notesMasterService {
	@Autowired
	notesMasterDao notesMasterDao;
	
	@Override
	public List<notesMaster> listNotesMaster() {
		
		return notesMasterDao.findAll();
		
	}

	@Override
	public notesMaster saveNotesMaster(notesMaster notesMaster) {
		
		return notesMasterDao.save(notesMaster);
		
	}

	@Override
	public notesMaster notesMasterXID(int id) {
		
		return notesMasterDao.findById(id).get();
		
	}

	@Override
	public notesMaster updateNotesMaster(notesMaster notesMaster) {
		
		return notesMasterDao.save(notesMaster);
		
	}

	@Override
	public void deleteNotesMaster(int id) {
		
		notesMasterDao.deleteById(id);
		
	}
}
