package com.theoltankard.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.theoltankard.dao.weaponsDao;
import com.theoltankard.dto.weapons;
@Service
public class weaponsImpl implements weaponsService {
	
	@Autowired
	weaponsDao weaponsDao;
	
	@Override
	public List<weapons> listarWeapons() {
		return weaponsDao.findAll();
	}

	@Override
	public weapons weaponsXID(int id) {
		return weaponsDao.findById(id).get();
	}

}

