package com.theoltankard.service;

import java.util.List;

import com.theoltankard.dto.notesMaster;

public interface notesMasterService {

	public List<notesMaster> listNotesMaster();
		
	public notesMaster saveNotesMaster(notesMaster note);
		
	public notesMaster notesMasterXID(int id);
		
	public notesMaster updateNotesMaster(notesMaster notesMaster);
		
	public void deleteNotesMaster(int id);

}
