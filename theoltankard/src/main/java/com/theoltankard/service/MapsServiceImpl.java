package com.theoltankard.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.theoltankard.dao.IMapsDAO;
import com.theoltankard.dto.Maps;

@Service
public class MapsServiceImpl implements IMapsService {

	@Autowired
	IMapsDAO iMapsDAO;
	
	@Override
	public List<Maps> listMaps() {

		return iMapsDAO.findAll();
		
	}

	@Override
	public Maps saveMap(Maps map) {

		return iMapsDAO.save(map);
		
	}

	@Override
	public Maps mapXID(int id) {

		return iMapsDAO.findById(id).get();
		
	}

	@Override
	public Maps updateMap(Maps map) {

		return iMapsDAO.save(map);
		
	}

	@Override
	public void deleteMap(int id) {

		iMapsDAO.deleteById(id);
		
	}

}
