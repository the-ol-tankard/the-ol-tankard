package com.theoltankard.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.theoltankard.dao.itemsDao;
import com.theoltankard.dto.items;

@Service
public class itemsImpl implements itemsService {
	
	@Autowired
	itemsDao itemsDao;
	
	@Override
	public List<items> listarItems() {
		return itemsDao.findAll();
	}

	@Override
	public items itemsXID(int id) {
		return itemsDao.findById(id).get();
	}

}

