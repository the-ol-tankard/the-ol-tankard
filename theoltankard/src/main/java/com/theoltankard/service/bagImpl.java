package com.theoltankard.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.theoltankard.dao.bagDao;
import com.theoltankard.dto.bag;

@Service
public class bagImpl implements bagService {
	@Autowired
	bagDao bagDao;
	
	@Override
	public List<bag> listarbag() {
		return bagDao.findAll();
	}

	@Override
	public bag guardarBag(bag bag) {
		return bagDao.save(bag);
	}

	@Override
	public bag bagXID(int id) {
		return bagDao.findById(id).get();
	}

	@Override
	public bag actualizarBag(bag bag) {
		return bagDao.save(bag);
	}

	@Override
	public void eliminarbag(int id) {
		bagDao.deleteById(id);
		
	}	
}

