package com.theoltankard.service;

import java.util.List;

import com.theoltankard.dto.characteroltankard;

public interface characteroltankardService {
	
	public List<characteroltankard> listarcharacteroltankard();
	
	public characteroltankard guardarcharacteroltankard(characteroltankard characteroltankard);
	
	public characteroltankard characteroltankardXID(int id);
	
	public characteroltankard actualizarcharacteroltankard(characteroltankard characteroltankard);
	
	public void eliminarcharacteroltankard(int id);

}
