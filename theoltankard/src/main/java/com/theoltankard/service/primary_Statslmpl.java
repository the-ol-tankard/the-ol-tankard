package com.theoltankard.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.theoltankard.dao.primary_StatsDao;
import com.theoltankard.dto.primary_Stats;

@Service
public class primary_Statslmpl implements primary_StatsService {
	
	@Autowired
	primary_StatsDao primary_StatsDao;

	@Override
	public List<primary_Stats> listarprimary_Stats() {
		return primary_StatsDao.findAll();
	}

	@Override
	public primary_Stats guardarprimary_Stats(primary_Stats primary_Stats) {
		return primary_StatsDao.save(primary_Stats);
	}

	@Override
	public primary_Stats primary_StatsXID(int id) {
		return primary_StatsDao.findById(id).get();
	}


}
