package com.theoltankard.service;

import java.util.List;

import com.theoltankard.dto.bag;

public interface bagService {
	public List<bag> listarbag(); //Listar All 
	
	public bag guardarBag(bag bag);	//Guarda un bag CREATE
	
	public bag bagXID(int id); //Leer datos de un bag READ
	
	public bag actualizarBag(bag bag); //Actualiza datos del bag UPDATE
	
	public void eliminarbag(int id);// Elimina el bag DELETE
}

