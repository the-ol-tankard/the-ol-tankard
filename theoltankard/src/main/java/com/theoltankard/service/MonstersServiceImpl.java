package com.theoltankard.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.theoltankard.dao.IMonstersDAO;
import com.theoltankard.dto.Monsters;

@Service
public class MonstersServiceImpl implements IMonstersService {

	@Autowired
	IMonstersDAO iMonstersDAO;
	
	@Override
	public List<Monsters> listMonsters() {

		return iMonstersDAO.findAll();
		
	}

	@Override
	public Monsters saveMonster(Monsters monster) {

		return iMonstersDAO.save(monster);
		
	}

	@Override
	public Monsters monsterXID(int id) {

		return iMonstersDAO.findById(id).get();
		
	}

	@Override
	public Monsters updateMonster(Monsters monster) {

		return iMonstersDAO.save(monster);
		
	}

	@Override
	public void deleteMonster(int id) {

		iMonstersDAO.deleteById(id);
		
	}

}
