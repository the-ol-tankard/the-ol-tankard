package com.theoltankard.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.theoltankard.dao.armorDao;
import com.theoltankard.dto.armor;

@Service
public class armorImpl implements armorService {
	
	@Autowired
	armorDao armorDao;
	
	@Override
	public List<armor> listarArmor() {
		return armorDao.findAll();
	}

	@Override
	public armor armorXID(int id) {
		return armorDao.findById(id).get();
	}

}

