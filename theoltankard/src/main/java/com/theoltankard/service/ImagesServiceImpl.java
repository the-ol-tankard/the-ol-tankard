package com.theoltankard.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.theoltankard.dao.IImagesDAO;
import com.theoltankard.dto.Images;

@Service
public class ImagesServiceImpl implements IImagesService {

	@Autowired
	IImagesDAO iImagesDAO;
	
	@Override
	public List<Images> listImages() {

		return iImagesDAO.findAll();
		
	}

	@Override
	public Images saveImage(Images image) {

		return iImagesDAO.save(image);
		
	}

	@Override
	public Images imageXID(int id) {

		return iImagesDAO.findById(id).get();
		
	}

	@Override
	public Images updateImage(Images image) {

		return iImagesDAO.save(image);
		
	}

	@Override
	public void deleteImage(int id) {

		iImagesDAO.deleteById(id);
		
	}

}
