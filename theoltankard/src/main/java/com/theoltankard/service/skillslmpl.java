package com.theoltankard.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.theoltankard.dto.skills;
import com.theoltankard.dao.skillsDao;


@Service
public class skillslmpl implements skillsService {
	
	@Autowired
	skillsDao skillsDao;

	@Override
	public List<skills> listarskills() {
		return skillsDao.findAll();
	}

	@Override
	public skills guardarskills(skills skills) {
		return skillsDao.save(skills);
	}

	@Override
	public skills skillsXID(int id) {
		return skillsDao.findById(id).get();
	}


}
