package com.theoltankard.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.theoltankard.dao.spellsDao;
import com.theoltankard.dto.spells;

@Service
public class spellsImpl implements spellsService {
	@Autowired
	spellsDao spellsDao;
	
	@Override
	public List<spells> listarSpells() {
		return spellsDao.findAll();
	}

	@Override
	public spells spellsXID(int id) {
		return spellsDao.findById(id).get();
	}

}

