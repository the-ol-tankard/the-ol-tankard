package com.theoltankard.service;

import java.util.List;

import com.theoltankard.dto.skills;


public interface skillsService {
	
	public List<skills> listarskills();
	
	public skills guardarskills(skills skills);
	
	public skills skillsXID(int id);
	

}
