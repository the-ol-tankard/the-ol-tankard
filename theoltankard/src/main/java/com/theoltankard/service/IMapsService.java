package com.theoltankard.service;

import java.util.List;

import com.theoltankard.dto.Maps;

public interface IMapsService {

	public List<Maps> listMaps();
	
	public Maps saveMap(Maps map);
	
	public Maps mapXID(int id);
	
	public Maps updateMap(Maps map);
	
	public void deleteMap(int id);

}
