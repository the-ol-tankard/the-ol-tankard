package com.theoltankard.service;

import java.util.List;

import com.theoltankard.dto.Monsters;

public interface IMonstersService {

	public List<Monsters> listMonsters();
	
	public Monsters saveMonster(Monsters monster);
	
	public Monsters monsterXID(int id);
	
	public Monsters updateMonster(Monsters monster);
	
	public void deleteMonster(int id);

}
