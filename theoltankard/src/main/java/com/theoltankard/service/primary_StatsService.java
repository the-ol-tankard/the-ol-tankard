package com.theoltankard.service;

import java.util.List;

import com.theoltankard.dto.primary_Stats;

public interface primary_StatsService {
	
	public List<primary_Stats> listarprimary_Stats();
	
	public primary_Stats guardarprimary_Stats(primary_Stats primary_Stats);
	
	public primary_Stats primary_StatsXID(int id);
	

}
