package com.theoltankard.service;

import java.util.List;

import com.theoltankard.dto.Notes;

public interface INotesService {

	public List<Notes> listNotes();
		
	public Notes saveNote(Notes note);
		
	public Notes noteXID(int id);
		
	public Notes updateNote(Notes note);
		
	public void deleteNote(int id);

}
