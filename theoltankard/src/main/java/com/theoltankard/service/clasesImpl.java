package com.theoltankard.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.theoltankard.dao.clasesDao;
import com.theoltankard.dto.clases;
@Service
public class clasesImpl implements clasesService {
	@Autowired
	clasesDao clasesDao;
	
	@Override
	public List<clases> listarClases() {
		return clasesDao.findAll();
	}

	@Override
	public clases clasesXID(int id) {
		return clasesDao.findById(id).get();
	}

}
