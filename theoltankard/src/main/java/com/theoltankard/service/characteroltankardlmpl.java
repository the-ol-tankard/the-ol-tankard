package com.theoltankard.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.theoltankard.dao.characteroltankardDao;
import com.theoltankard.dto.characteroltankard;


@Service
public class characteroltankardlmpl implements characteroltankardService {
	
	@Autowired
	characteroltankardDao characteroltankardDAO;

	@Override
	public List<characteroltankard> listarcharacteroltankard() {
		return characteroltankardDAO.findAll();
	}

	@Override
	public characteroltankard guardarcharacteroltankard(characteroltankard characteroltankard) {
		return characteroltankardDAO.save(characteroltankard);
	}

	@Override
	public characteroltankard characteroltankardXID(int id) {
		return characteroltankardDAO.findById(id).get();
	}

	@Override
	public characteroltankard actualizarcharacteroltankard(characteroltankard characteroltankard) {
		return characteroltankardDAO.save(characteroltankard);
	}

	@Override
	public void eliminarcharacteroltankard(int id) {
		characteroltankardDAO.deleteById(id);
	}

}
