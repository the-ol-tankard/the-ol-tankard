package com.theoltankard.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.theoltankard.dao.INotesDAO;
import com.theoltankard.dto.Notes;

@Service
public class NotesServiceImpl implements INotesService {

	@Autowired
	INotesDAO iNotesDAO;
	
	@Override
	public List<Notes> listNotes() {
		
		return iNotesDAO.findAll();
		
	}

	@Override
	public Notes saveNote(Notes note) {
		
		return iNotesDAO.save(note);
		
	}

	@Override
	public Notes noteXID(int id) {
		
		return iNotesDAO.findById(id).get();
		
	}

	@Override
	public Notes updateNote(Notes note) {
		
		return iNotesDAO.save(note);
		
	}

	@Override
	public void deleteNote(int id) {
		
		iNotesDAO.deleteById(id);
		
	}

}
