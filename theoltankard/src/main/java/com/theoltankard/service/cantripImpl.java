package com.theoltankard.service;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.theoltankard.dao.cantripDao;
import com.theoltankard.dto.cantrip;
@Service
public class cantripImpl implements cantripService {
	
	@Autowired
	cantripDao cantripDao;
	
	@Override
	public List<cantrip> listarCantrip() {
		return cantripDao.findAll();
	}

	@Override
	public cantrip cantripXID(int id) {
		return cantripDao.findById(id).get();
	}

}
