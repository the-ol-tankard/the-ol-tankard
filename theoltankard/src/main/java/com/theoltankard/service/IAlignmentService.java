package com.theoltankard.service;

import java.util.List;

import com.theoltankard.dto.Alignment;

public interface IAlignmentService {

	public List<Alignment> listAlignment();
	
	public Alignment saveAlignment(Alignment alignment);
	
	public Alignment alignmentXID(int id);
	
	public Alignment updateAlignment(Alignment alignment);
	
	public void deleteAlignment(int id);

}
