package com.theoltankard.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.theoltankard.dao.IAlignmentDAO;
import com.theoltankard.dto.Alignment;

@Service
public class AlignmentServiceImpl implements IAlignmentService {

	@Autowired
	IAlignmentDAO iAlignmentDAO;
	
	@Override
	public List<Alignment> listAlignment() {

		return iAlignmentDAO.findAll();
		
	}

	@Override
	public Alignment saveAlignment(Alignment alignment) {

		return iAlignmentDAO.save(alignment);
		
	}

	@Override
	public Alignment alignmentXID(int id) {

		return iAlignmentDAO.findById(id).get();
		
	}

	@Override
	public Alignment updateAlignment(Alignment alignment) {

		return iAlignmentDAO.save(alignment);
		
	}

	@Override
	public void deleteAlignment(int id) {

		iAlignmentDAO.deleteById(id);
		
	}

}
