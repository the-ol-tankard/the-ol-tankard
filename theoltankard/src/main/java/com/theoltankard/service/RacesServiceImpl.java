package com.theoltankard.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.theoltankard.dao.IRacesDAO;
import com.theoltankard.dto.Races;

@Service
public class RacesServiceImpl implements IRacesService {

	@Autowired
	IRacesDAO iRacesDAO;
	
	@Override
	public List<Races> listRaces() {

		return iRacesDAO.findAll();
		
	}

	@Override
	public Races saveRace(Races race) {

		return iRacesDAO.save(race);
		
	}

	@Override
	public Races raceXID(int id) {

		return iRacesDAO.findById(id).get();
		
	}

	@Override
	public Races updateRace(Races race) {

		return iRacesDAO.save(race);
		
	}

	@Override
	public void deleteRace(int id) {

		iRacesDAO.deleteById(id);
		
	}

}
