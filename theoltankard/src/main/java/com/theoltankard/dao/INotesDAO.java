package com.theoltankard.dao;

import org.springframework.data.jpa.repository.JpaRepository;

import com.theoltankard.dto.Notes;

public interface INotesDAO extends JpaRepository<Notes, Integer> {
	
}