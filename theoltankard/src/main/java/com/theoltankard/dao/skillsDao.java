package com.theoltankard.dao;

import org.springframework.data.jpa.repository.JpaRepository;
import com.theoltankard.dto.skills;

public interface skillsDao extends JpaRepository<skills, Integer> {

}
