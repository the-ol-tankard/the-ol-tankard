package com.theoltankard.dao;

import org.springframework.data.jpa.repository.JpaRepository;
import com.theoltankard.dto.notesMaster;

public interface notesMasterDao extends JpaRepository<notesMaster, Integer> {

}
