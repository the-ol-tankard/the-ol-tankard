package com.theoltankard.dao;

import org.springframework.data.jpa.repository.JpaRepository;
import com.theoltankard.dto.weapons;

public interface weaponsDao extends JpaRepository<weapons, Integer> {

}

