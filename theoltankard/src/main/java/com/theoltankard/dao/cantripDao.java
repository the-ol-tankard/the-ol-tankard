package com.theoltankard.dao;

import org.springframework.data.jpa.repository.JpaRepository;
import com.theoltankard.dto.cantrip;

public interface cantripDao extends JpaRepository<cantrip, Integer> {

}
