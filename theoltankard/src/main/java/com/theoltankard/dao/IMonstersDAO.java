package com.theoltankard.dao;

import org.springframework.data.jpa.repository.JpaRepository;

import com.theoltankard.dto.Monsters;

public interface IMonstersDAO extends JpaRepository<Monsters, Integer> {
	
}
