package com.theoltankard.dao;

import org.springframework.data.jpa.repository.JpaRepository;

import com.theoltankard.dto.primary_Stats;


public interface primary_StatsDao extends JpaRepository<primary_Stats, Integer> {

}