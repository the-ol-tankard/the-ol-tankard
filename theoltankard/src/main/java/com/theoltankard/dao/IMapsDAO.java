package com.theoltankard.dao;

import org.springframework.data.jpa.repository.JpaRepository;

import com.theoltankard.dto.Maps;

public interface IMapsDAO extends JpaRepository<Maps, Integer> {
	
}
