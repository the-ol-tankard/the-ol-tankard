package com.theoltankard.dao;

import org.springframework.data.jpa.repository.JpaRepository;

import com.theoltankard.dto.Races;

public interface IRacesDAO extends JpaRepository<Races, Integer> {
	
}
