package com.theoltankard.dao;

import org.springframework.data.jpa.repository.JpaRepository;
import com.theoltankard.dto.spells;

public interface spellsDao extends JpaRepository<spells, Integer> {

}

