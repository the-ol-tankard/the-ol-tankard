package com.theoltankard.dao;

import org.springframework.data.jpa.repository.JpaRepository;

import com.theoltankard.dto.Images;


public interface IImagesDAO extends JpaRepository<Images, Integer> {
	
}
