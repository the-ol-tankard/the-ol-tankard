package com.theoltankard.dao;

import org.springframework.data.jpa.repository.JpaRepository;
import com.theoltankard.dto.items;

public interface itemsDao extends JpaRepository<items, Integer> {

}

