package com.theoltankard.dao;

import org.springframework.data.jpa.repository.JpaRepository;

import com.theoltankard.dto.Alignment;

public interface IAlignmentDAO extends JpaRepository<Alignment, Integer> {
	
}
