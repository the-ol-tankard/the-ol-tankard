package com.theoltankard.dao;


import org.springframework.data.jpa.repository.JpaRepository;
import com.theoltankard.dto.usuario;

public interface usuarioDao extends JpaRepository<usuario, Long> {

	usuario findByUsername(String username);
}

