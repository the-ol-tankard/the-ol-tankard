package com.theoltankard.dto;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="maps")
public class Maps {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private int id;
	
	@Column(name = "name")
	private String name;
	
	@Column(name = "content")
	private String content;
	
	@Column(name = "date")
	private int date;
	
	public Maps() {
		
	}
	
	/**
	 * @param id
	 * @param name
	 * @param content
	 * @param date
	 */
	public Maps(int id, String name, String content, int date) {
		
		super();
		this.id = id;
		this.name = name;
		this.content = content;
		this.date = date;
		
	}

	/**
	 * @return the id
	 */
	public int getId() {
		
		return id;
		
	}

	/**
	 * @param id the id to set
	 */
	public void setId(int id) {
		
		this.id = id;
		
	}

	/**
	 * @return the name
	 */
	public String getName() {
		
		return name;
		
	}

	/**
	 * @param name the name to set
	 */
	public void setName(String name) {
		
		this.name = name;
		
	}
	
	/**
	 * @return the content
	 */
	public String getContent() {
		
		return content;
		
	}

	/**
	 * @param content the content to set
	 */
	public void setContent(String content) {
		
		this.content = content;
		
	}
	
	/**
	 * @return the date
	 */
	public int getDate() {
		
		return date;
		
	}

	/**
	 * @param date the date to set
	 */
	public void setDate(int date) {
		
		this.date = date;
		
	}

	@Override
	public String toString() {
		
		return "Notes [id=" + id + ", name=" + name + ", content=" + content + ", date=" + date + "]";
		
	}
		
}
