package com.theoltankard.dto;

import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;
import javax.persistence.Table;


@Entity
@Table(name="armor")
public class armor {
	
	//Creación de las variables del objeto
		@Id
		@GeneratedValue(strategy = GenerationType.IDENTITY)
		private Integer id;
		@Column(name = "nom")
		private String nom;
		@Column(name = "ac")
		private int ac;
		@Column(name = "cost")
		private int cost;
		
		@OneToMany
		@JoinColumn(name="id")
		private List<bag> bag;

		//Los gets de clases
		public Integer getId() {
			return id;
		}

		public String getNom() {
			return nom;
		}

		public int getAc() {
			return ac;
		}

		public int getCost() {
			return cost;
		}

		public List<bag> getBag() {
			return bag;
		}

		@Override
		public String toString() {
			return "armor [id=" + id + ", nom=" + nom + ", ac=" + ac + ", cost=" + cost + "]";
		}


		
}
