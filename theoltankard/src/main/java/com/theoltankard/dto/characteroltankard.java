package com.theoltankard.dto;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="characteroltankard")
public class characteroltankard {
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private int id;
	
	@Column(name = "name")
	private String name;
	
	@Column(name = "hp")
	private int hp;
	
	@Column(name = "class")
	private int classes;
	
	@Column(name = "subclassid")
	private int subclassid;
	
	@Column(name = "race")
	private int race;
	
	@Column(name = "alignment")
	private int alignment;
	
	@Column(name = "background")
	private String background;
	
	@Column(name = "age")
	private int age;
	
	@Column(name = "size")
	private double size;
	
	@Column(name = "primarycharacter")
	private int primaryCharacter;
	
	@Column(name = "skillscharacter")
	private int skillsCharacter;
	
	public characteroltankard() {
		
	}

	public characteroltankard(int id, String name, int hp, int classes, int subclassid, int race, int alignment,
			String background, int age, double size, int primaryCharacter, int skillsCharacter) {
		super();
		this.id = id;
		this.name = name;
		this.hp = hp;
		this.classes = classes;
		this.subclassid = subclassid;
		this.race = race;
		this.alignment = alignment;
		this.background = background;
		this.age = age;
		this.size = size;
		this.primaryCharacter = primaryCharacter;
		this.skillsCharacter = skillsCharacter;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public int getHp() {
		return hp;
	}

	public void setHp(int hp) {
		this.hp = hp;
	}

	public int getClasses() {
		return classes;
	}

	public void setClasses(int classes) {
		this.classes = classes;
	}

	public int getSubclassid() {
		return subclassid;
	}

	public void setSubclassid(int subclassid) {
		this.subclassid = subclassid;
	}

	public int getRace() {
		return race;
	}

	public void setRace(int race) {
		this.race = race;
	}

	public int getAlignment() {
		return alignment;
	}

	public void setAlignment(int alignment) {
		this.alignment = alignment;
	}

	public String getBackground() {
		return background;
	}

	public void setBackground(String background) {
		this.background = background;
	}

	public int getAge() {
		return age;
	}

	public void setAge(int age) {
		this.age = age;
	}

	public double getSize() {
		return size;
	}

	public void setSize(double size) {
		this.size = size;
	}

	public int getPrimaryCharacter() {
		return primaryCharacter;
	}

	public void setPrimaryCharacter(int primaryCharacter) {
		this.primaryCharacter = primaryCharacter;
	}

	public int getSkillsCharacter() {
		return skillsCharacter;
	}

	public void setSkillsCharacter(int skillsCharacter) {
		this.skillsCharacter = skillsCharacter;
	}

	@Override
	public String toString() {
		return "characteroltankard [id=" + id + ", name=" + name + ", hp=" + hp + ", classes=" + classes
				+ ", subclassid=" + subclassid + ", race=" + race + ", alignment=" + alignment + ", background="
				+ background + ", age=" + age + ", size=" + size + ", primaryCharacter=" + primaryCharacter
				+ ", skillsCharacter=" + skillsCharacter + "]";
	}
	
	

}


