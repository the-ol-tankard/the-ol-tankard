package com.theoltankard.dto;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="images")
public class Images {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private int id;
	
	@Column(name = "name")
	private String name;
	
	public Images() {
		
	}
	
	/**
	 * @param id
	 * @param name
	 */
	public Images(int id, String name) {
		
		super();
		this.id = id;
		this.name = name;
		
	}

	/**
	 * @return the id
	 */
	public int getId() {
		
		return id;
		
	}

	/**
	 * @param id the id to set
	 */
	public void setId(int id) {
		
		this.id = id;
		
	}

	/**
	 * @return the name
	 */
	public String getName() {
		
		return name;
		
	}

	/**
	 * @param name the name to set
	 */
	public void setName(String name) {
		
		this.name = name;
		
	}

	@Override
	public String toString() {
		
		return "Images [id=" + id + ", name=" + name + "]";
		
	}
		
}
