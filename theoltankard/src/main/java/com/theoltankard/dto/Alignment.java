package com.theoltankard.dto;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="alignment")
public class Alignment {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private int id;
	
	@Column(name = "tipo")
	private String Tipo;
	
	public Alignment() {
		
	}
	
	/**
	 * @param id
	 * @param Tipo
	 */
	public Alignment(int id, String Tipo) {
		
		super();
		this.id = id;
		this.Tipo = Tipo;
		
	}

	/**
	 * @return the id
	 */
	public int getId() {
		
		return id;
		
	}

	/**
	 * @param id the id to set
	 */
	public void setId(int id) {
		
		this.id = id;
		
	}

	/**
	 * @return the tipo
	 */
	public String getTipo() {
		
		return Tipo;
		
	}

	/**
	 * @param name the name to set
	 */
	public void setTipo(String Tipo) {
		
		this.Tipo = Tipo;
		
	}

	@Override
	public String toString() {
		
		return "Alignment [id=" + id + ", Tipo=" + Tipo + "]";
		
	}
		
}
