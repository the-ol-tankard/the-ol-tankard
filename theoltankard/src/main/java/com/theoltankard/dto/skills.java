package com.theoltankard.dto;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="skills")
public class skills {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private int id;
	
	@Column(name = "athletics")
	private int athletics;
	
	@Column(name = "acrobatics")
	private int acrobatics;
	
	@Column(name = "sleight_of_hand")
	private int sleight_of_hand;
	
	@Column(name = "stealth")
	private int stealth;
	
	@Column(name = "arcana")
	private int arcana;
	
	@Column(name = "history")
	private int history;
	
	@Column(name = "investigation")
	private int investigation;
	
	@Column(name = "nature")
	private int nature;
	
	@Column(name = "religion")
	private int religion;
	
	@Column(name = "animal_handling")
	private int animal_handling;
	
	@Column(name = "insight")
	private int insight;
	
	@Column(name = "medicine")
	private int medicine;
	
	@Column(name = "perception")
	private int perception;
	
	@Column(name = "survival")
	private int survival;
	
	@Column(name = "deception")
	private int deception;
	
	@Column(name = "intimidation")
	private int intimidation;
	
	@Column(name = "performance")
	private int performance;
	
	@Column(name = "persuasion")
	private int persuasion;
	

	public skills() {
		
	}


	public skills(int id, int athletics, int acrobatics, int sleight_of_hand, int stealth, int arcana, int history,
			int investigation, int nature, int religion, int animal_handling, int insight, int medicine, int perception,
			int survival, int deception, int intimidation, int performance, int persuasion) {
		super();
		this.id = id;
		this.athletics = athletics;
		this.acrobatics = acrobatics;
		this.sleight_of_hand = sleight_of_hand;
		this.stealth = stealth;
		this.arcana = arcana;
		this.history = history;
		this.investigation = investigation;
		this.nature = nature;
		this.religion = religion;
		this.animal_handling = animal_handling;
		this.insight = insight;
		this.medicine = medicine;
		this.perception = perception;
		this.survival = survival;
		this.deception = deception;
		this.intimidation = intimidation;
		this.performance = performance;
		this.persuasion = persuasion;
	}


	@Override
	public String toString() {
		return "skills [id=" + id + ", athletics=" + athletics + ", acrobatics=" + acrobatics + ", sleight_of_hand="
				+ sleight_of_hand + ", stealth=" + stealth + ", arcana=" + arcana + ", history=" + history
				+ ", investigation=" + investigation + ", nature=" + nature + ", religion=" + religion
				+ ", animal_handling=" + animal_handling + ", insight=" + insight + ", medicine=" + medicine
				+ ", perception=" + perception + ", survival=" + survival + ", deception=" + deception
				+ ", intimidation=" + intimidation + ", performance=" + performance + ", persuasion=" + persuasion
				+ "]";
	}
	
	
	
	
}
