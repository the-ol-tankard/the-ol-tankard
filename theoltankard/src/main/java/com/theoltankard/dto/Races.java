package com.theoltankard.dto;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="races")
public class Races {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private int id;
	
	@Column(name = "name")
	private String name;
	
	@Column(name = "speed")
	private String Speed;
	
	@Column(name = "description")
	private String Description;
	
	@Column(name = "abilityscore")
	private String AbilityScore;
	
	@Column(name = "trait1")
	private String Trait1;
	
	@Column(name = "trait2")
	private String Trait2;
	
	@Column(name = "trait3")
	private String Trait3;
	
	public Races() {
		
	}
	
	/**
	 * @param id
	 * @param name
	 * @param Speed
	 * @param Description
	 * @param AbilityScore
	 * @param Trait1
	 * @param Trait2
	 * @param Trait3
	 */
	public Races(int id, String name, String Speed, String Description, String AbilityScore, String Trait1, String Trait2, String Trait3) {
		
		super();
		this.id = id;
		this.name = name;
		this.Speed = Speed;
		this.Description = Description;
		this.AbilityScore = AbilityScore;
		this.Trait1 = Trait1;
		this.Trait2 = Trait2;
		this.Trait3 = Trait3;
		
	}

	/**
	 * @return the id
	 */
	public int getId() {
		
		return id;
		
	}

	/**
	 * @param id the id to set
	 */
	public void setId(int id) {
		
		this.id = id;
		
	}

	/**
	 * @return the name
	 */
	public String getName() {
		
		return name;
		
	}

	/**
	 * @param name the name to set
	 */
	public void setName(String name) {
		
		this.name = name;
		
	}
	
	/**
	 * @return the speed
	 */
	public String getSpeed() {
		
		return Speed;
		
	}

	/**
	 * @param speed the speed to set
	 */
	public void setSpeed(String Speed) {
		
		this.Speed = Speed;
		
	}
	
	/**
	 * @return the description
	 */
	public String getDescription() {
		
		return Description;
		
	}

	/**
	 * @param description the description to set
	 */
	public void setDescription(String Description) {
		
		this.Description = Description;
		
	}
	
	/**
	 * @return the abilityscore
	 */
	public String getAbilityScore() {
		
		return AbilityScore;
		
	}

	/**
	 * @param abilityscore the abilityscore to set
	 */
	public void setAbilityScore(String AbilityScore) {
		
		this.AbilityScore = AbilityScore;
		
	}
	
	/**
	 * @return the trait1
	 */
	public String getTrait1() {
		
		return Trait1;
		
	}

	/**
	 * @param trait1 the trait1 to set
	 */
	public void setTrait1(String Trait1) {
		
		this.Trait1 = Trait1;
		
	}
	
	/**
	 * @return the trait2
	 */
	public String getTrait2() {
		
		return Trait2;
		
	}

	/**
	 * @param trait2 the trait2 to set
	 */
	public void setTrait2(String Trait2) {
		
		this.Trait2 = Trait2;
		
	}
	
	/**
	 * @return the trait3
	 */
	public String getTrait3() {
		
		return Trait3;
		
	}

	/**
	 * @param trait3 the trait3 to set
	 */
	public void setTrait3(String Trait3) {
		
		this.Trait3 = Trait3;
		
	}

	@Override
	public String toString() {
		
		return "Races [id=" + id + ", name=" + name + ", Speed=" + Speed + ", Description=" + Description
				+ ", AbilityScore=" + AbilityScore + ", Trait1=" + Trait1 + ", Trait2=" + Trait2 + ", Trait3=" + Trait3
				+ "]";
		
	}
		
}
