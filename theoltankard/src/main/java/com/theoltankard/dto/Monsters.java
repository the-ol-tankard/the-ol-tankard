package com.theoltankard.dto;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="monsters")
public class Monsters {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private int id;
	
	@Column(name = "name")
	private String name;
	
	@Column(name = "description")
	private String description;
	
	@Column(name = "armor_class")
	private int armor_class;
	
	@Column(name = "hit_points")
	private int hit_points;
	
	@Column(name = "attack")
	private String attack;
	
	@Column(name = "speed")
	private int speed;
	
	@Column(name = "str")
	private int str;
	
	@Column(name = "dex")
	private int dex;
	
	@Column(name = "con")
	private int con;
	
	@Column(name = "inte")
	private int inte;
	
	@Column(name = "wis")
	private int wis;
	
	@Column(name = "cha")
	private int cha;
	
	public Monsters() {
		
	}
	
	/**
	 * @param id
	 * @param name
	 * @param description
	 * @param armor_class
	 * @param hit_points
	 * @param attack
	 * @param speed
	 * @param str
	 * @param dex
	 * @param con
	 * @param inte
	 * @param wis
	 * @param cha
	 */
	public Monsters(int id, String name, String description, int armor_class, int hit_points, String attack, int speed, int str, int dex, int con, int inte, int wis, int cha) {
		
		super();
		this.id = id;
		this.name = name;
		this.description = description;
		this.armor_class = armor_class;
		this.hit_points = hit_points;
		this.attack = attack;
		this.speed = speed;
		this.str = str;
		this.dex = dex;
		this.con = con;
		this.inte = inte;
		this.wis = wis;
		this.cha = cha;
		
	}

	/**
	 * @return the id
	 */
	public int getId() {
		
		return id;
		
	}

	/**
	 * @param id the id to set
	 */
	public void setId(int id) {
		
		this.id = id;
		
	}

	/**
	 * @return the name
	 */
	public String getName() {
		
		return name;
		
	}

	/**
	 * @param name the name to set
	 */
	public void setName(String name) {
		
		this.name = name;
		
	}
	
	/**
	 * @return the description
	 */
	public String getDescription() {
		
		return description;
		
	}

	/**
	 * @param description the description to set
	 */
	public void setDescription(String description) {
		
		this.description = description;
		
	}
	
	/**
	 * @return the armor_class
	 */
	public int getArmorClass() {
		
		return armor_class;
		
	}

	/**
	 * @param armor_class the armor_class to set
	 */
	public void setArmorClass(int armor_class) {
		
		this.armor_class = armor_class;
		
	}
	
	/**
	 * @return the hit_points
	 */
	public int getHitPoints() {
		
		return hit_points;
		
	}

	/**
	 * @param hit_points the hit_points to set
	 */
	public void setHitPoints(int hit_points) {
		
		this.hit_points = hit_points;
		
	}
	
	/**
	 * @return the attack
	 */
	public String getAttack() {
		
		return attack;
		
	}

	/**
	 * @param attack the attack to set
	 */
	public void setAttack(String attack) {
		
		this.attack = attack;
		
	}
	
	/**
	 * @return the speed
	 */
	public int getSpeed() {
		
		return speed;
		
	}

	/**
	 * @param speed the speed to set
	 */
	public void setSpeed(int speed) {
		
		this.speed = speed;
		
	}
	
	/**
	 * @return the str
	 */
	public int getStr() {
		
		return str;
		
	}

	/**
	 * @param str the str to set
	 */
	public void setStr(int str) {
		
		this.str = str;
		
	}
	
	/**
	 * @return the dex
	 */
	public int getDex() {
		
		return dex;
		
	}

	/**
	 * @param dex the dex to set
	 */
	public void setDex(int dex) {
		
		this.dex = dex;
		
	}
	
	/**
	 * @return the con
	 */
	public int getCon() {
		
		return con;
		
	}

	/**
	 * @param con the con to set
	 */
	public void setCon(int con) {
		
		this.con = con;
		
	}
	
	/**
	 * @return the inte
	 */
	public int getInte() {
		
		return inte;
		
	}

	/**
	 * @param inte the inte to set
	 */
	public void setInte(int inte) {
		
		this.inte = inte;
		
	}
	
	/**
	 * @return the wis
	 */
	public int getWis() {
		
		return wis;
		
	}

	/**
	 * @param wis the wis to set
	 */
	public void setWis(int wis) {
		
		this.wis = wis;
		
	}
	
	/**
	 * @return the cha
	 */
	public int getCha() {
		
		return cha;
		
	}

	/**
	 * @param cha the cha to set
	 */
	public void setCha(int cha) {
		
		this.cha = cha;
		
	}

	@Override
	public String toString() {
		
		return "Monsters [id=" + id + ", name=" + name + ", description=" + description + ", armor_class=" + armor_class
				+ ", hit_points=" + hit_points + ", attack=" + attack + ", speed=" + speed + ", str=" + str + ", dex="
				+ dex + ", con=" + con + ", inte=" + inte + ", wis=" + wis + ", cha=" + cha + "]";
		
	}
		
}
