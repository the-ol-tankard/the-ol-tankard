package com.theoltankard.dto;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="primary_stats")
public class primary_Stats {
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private int id;
	
	@Column(name = "str")
	private int str;
	
	@Column(name = "dex")
	private int dex;
	
	@Column(name = "con")
	private int con;
	
	@Column(name = "inte")
	private int inte;
	
	@Column(name = "wis")
	private int wis;
	
	@Column(name = "cha")
	private int cha;
	
	public primary_Stats() {
		
	}
	
	public primary_Stats(int id, int str, int dex, int con, int inte, int wis, int cha) {
		super();
		this.id = id;
		this.str = str;
		this.dex = dex;
		this.con = con;
		this.inte = inte;
		this.wis = wis;
		this.cha = cha;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public int getStr() {
		return str;
	}

	public void setStr(int str) {
		this.str = str;
	}

	public int getDex() {
		return dex;
	}

	public void setDex(int dex) {
		this.dex = dex;
	}

	public int getCon() {
		return con;
	}

	public void setCon(int con) {
		this.con = con;
	}

	public int getInte() {
		return inte;
	}

	public void setInte(int inte) {
		this.inte = inte;
	}

	public int getWis() {
		return wis;
	}

	public void setWis(int wis) {
		this.wis = wis;
	}

	public int getCha() {
		return cha;
	}

	public void setCha(int cha) {
		this.cha = cha;
	}

	@Override
	public String toString() {
		return "primary_Stats [id=" + id + ", str=" + str + ", dex=" + dex + ", con=" + con + ", inte=" + inte
				+ ", wis=" + wis + ", cha=" + cha + "]";
	}
	
	
	
	

}
