package com.theoltankard.dto;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

//Associo con la tabla correspondiente en el sql
@Entity
@Table(name="clases")
public class clases {
	
	//Creación de las variables del objeto
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Integer id;
	@Column(name = "name")
	private String name;
	@Column(name = "description")
	private String description;
	@Column(name = "attack")
	private String attack;
	
	@ManyToOne(optional=false)
    @JoinColumn(name="subclass1", nullable=false)
    private subclass subclass1;
	@ManyToOne(optional=false)
    @JoinColumn(name="subclass2", nullable=false)
    private subclass subclass2;
	
	@ManyToOne(optional=false)
    @JoinColumn(name="spell1", nullable=false)
    private spells spell1;
	@ManyToOne(optional=false)
    @JoinColumn(name="spell2", nullable=false)
    private spells spell2;
	
	@ManyToOne(optional=false)
    @JoinColumn(name="cantrip1", nullable=false)
    private cantrip cantrip1;
	@ManyToOne(optional=false)
    @JoinColumn(name="cantrip2", nullable=false)
    private cantrip cantrip2;
	
	//Los gets de clases
	public Integer getId() {
		return id;
	}
	public String getName() {
		return name;
	}
	public String getDescription() {
		return description;
	}
	public String getAttack() {
		return attack;
	}
	public subclass getSubclass1() {
		return subclass1;
	}
	public subclass getSubclass2() {
		return subclass2;
	}
	public spells getSpell1() {
		return spell1;
	}
	public spells getSpell2() {
		return spell2;
	}
	public cantrip getCantrip1() {
		return cantrip1;
	}
	public cantrip getCantrip2() {
		return cantrip2;
	}
	@Override
	public String toString() {
		return "clases [id=" + id + ", name=" + name + ", description=" + description + ", attack=" + attack
				+ ", cantrip1=" + cantrip1 + ", cantrip2=" + cantrip2 + "]";
	}
	
	

}
