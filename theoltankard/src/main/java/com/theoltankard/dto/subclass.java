package com.theoltankard.dto;

import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

//Associo con la tabla correspondiente en el sql
@Entity
@Table(name="subclass")
public class subclass {
	
	//Creación de las variables del objeto
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Integer id;
	@Column(name = "name")
	private String name;
	@Column(name = "description")
	private String description;
	@Column(name = "trait1")
	private String trait1;
	@Column(name = "trait1desc")
	private String trait1Desc;
	@Column(name = "trait2")
	private String trait2;
	@Column(name = "trait2desc")
	private String trait2Desc;
	
	//Una clave foranea hacia clases
	@OneToMany(mappedBy="subclass1")
	private Set<clases> clases1;
	@OneToMany(mappedBy="subclass2")
	private Set<clases> clases2;
	

	//Los gets del objeto
	public int getId() {
		return id;
	}

	public String getName() {
		return name;
	}

	public String getDescription() {
		return description;
	}

	public String getTrait1() {
		return trait1;
	}

	public String getTrait1Desc() {
		return trait1Desc;
	}

	public String getTrait2() {
		return trait2;
	}

	public String getTrait2Desc() {
		return trait2Desc;
	}

	@Override
	public String toString() {
		return "subclass [id=" + id + ", name=" + name + ", description=" + description + ", trait1=" + trait1
				+ ", trait1Desc=" + trait1Desc + ", trait2=" + trait2 + ", trait2Desc=" + trait2Desc + ", clases1="
				+ clases1 + ", clases2=" + clases2 + "]";
	}

	
	
}
