package com.theoltankard.dto;

import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

//Associo con la tabla correspondiente en el sql
@Entity
@Table(name="spells")
public class spells {
	
	//Creación de las variables del objeto
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Integer id;
	@Column(name = "name")
	private String name;
	@Column(name = "type")
	private String type;
	@Column(name = "dice")
	private String dice;
	@Column(name = "slot")
	private String slot;
	@Column(name = "description")
	private String description;
	@Column(name = "referencedclass")
	private String referencedClass;
	
	//Una clave foranea hacia clases
	@OneToMany(mappedBy="spell1")
	private Set<clases> clases1;
	@OneToMany(mappedBy="spell2")
	private Set<clases> clases2;

	//Los gets del objeto
	public int getId() {
		return id;
	}

	public String getName() {
		return name;
	}

	public String getType() {
		return type;
	}

	public String getDice() {
		return dice;
	}

	public String getSlot() {
		return slot;
	}

	public String getDescription() {
		return description;
	}

	public String getReferencedClass() {
		return referencedClass;
	}

	@Override
	public String toString() {
		return "spells [id=" + id + ", name=" + name + ", type=" + type + ", dice=" + dice + ", slot=" + slot
				+ ", description=" + description + ", referencedClass=" + referencedClass + ", clases1=" + clases1
				+ ", clases2=" + clases2 + "]";
	}

	

}
