package com.theoltankard.dto;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;


@Entity
@Table(name="bag")
public class bag {
	//Creación de las variables del objeto
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Integer id;
	@Column(name = "quantity")
	private int quantity;
	@Column(name = "player")
	private String player;
	
	@ManyToOne
	@JoinColumn(name="item")
	private items item;
	
	@ManyToOne
	@JoinColumn(name="armor")
	private armor armor;
	
	@ManyToOne
	@JoinColumn(name="weapon")
	private weapons weapon;
	
	//Los gets de clases
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public int getQuantity() {
		return quantity;
	}
	public void setQuantity(int quantity) {
		this.quantity = quantity;
	}
	public String getPlayer() {
		return player;
	}
	public void setPlayer(String player) {
		this.player = player;
	}
	public items getItems() {
		return item;
	}
	public void setItems(items items) {
		this.item = items;
	}
	public armor getArmor() {
		return armor;
	}
	public void setArmor(armor armor) {
		this.armor = armor;
	}
	public weapons getWeapons() {
		return weapon;
	}
	public void setWeapons(weapons weapons) {
		this.weapon = weapons;
	}
	
	@Override
	public String toString() {
		return "bag [id=" + id + ", quantity=" + quantity + ", player=" + player + ", item=" + item + ", armor=" + armor
				+ "]";
	}
	
	
}

