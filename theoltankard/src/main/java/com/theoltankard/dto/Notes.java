package com.theoltankard.dto;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="notes")
public class Notes {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private int id;
	
	@Column(name = "name")
	private String name;
	
	@Column(name = "text")
	private String text;
	
	@Column(name = "date")
	private int date;
	
	public Notes() {
		
	}
	
	/**
	 * @param id
	 * @param name
	 * @param text
	 * @param date
	 */
	public Notes(int id, String name, String text, int date) {
		
		super();
		this.id = id;
		this.name = name;
		this.text = text;
		this.date = date;
		
	}

	/**
	 * @return the id
	 */
	public int getId() {
		
		return id;
		
	}

	/**
	 * @param id the id to set
	 */
	public void setId(int id) {
		
		this.id = id;
		
	}

	/**
	 * @return the name
	 */
	public String getName() {
		
		return name;
		
	}

	/**
	 * @param name the name to set
	 */
	public void setName(String name) {
		
		this.name = name;
		
	}
	
	/**
	 * @return the text
	 */
	public String getText() {
		
		return text;
		
	}

	/**
	 * @param text the text to set
	 */
	public void setText(String text) {
		
		this.text = text;
		
	}
	
	/**
	 * @return the date
	 */
	public int getDate() {
		
		return date;
		
	}

	/**
	 * @param date the date to set
	 */
	public void setDate(int date) {
		
		this.date = date;
		
	}

	@Override
	public String toString() {
		
		return "Notes [id=" + id + ", name=" + name + ", text=" + text + ", date=" + date + "]";
		
	}
		
}
