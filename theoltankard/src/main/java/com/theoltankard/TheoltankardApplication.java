package com.theoltankard;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class TheoltankardApplication {

	public static void main(String[] args) {
		SpringApplication.run(TheoltankardApplication.class, args);
	}

}
