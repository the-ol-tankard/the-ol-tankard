import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { CharacterDiceComponent } from './jugador/character-dice/character-dice.component';
import { CharacterCreationComponent } from './jugador/jugador/crud/character-creation/character-creation.component';
import { CharacterInformationComponent } from './jugador/jugador/crud/character-information/character-information.component';
import { CharacterViewerComponent } from './jugador/jugador/crud/character-viewer/character-viewer.component';
import { UpdateCharacterComponent } from './jugador/jugador/crud/update-character/update-character.component';
import { MenuprincipalPlayerComponent } from './jugador/menuprincipal-player/menuprincipal-player.component';
import { CreateNotesJugadorComponent } from './jugador/notes-jugador/crud/create-notes-jugador/create-notes-jugador.component';
import { ListNotesJugadorComponent } from './jugador/notes-jugador/crud/list-notes-jugador/list-notes-jugador.component';
import { UpdateNotesJugadorComponent } from './jugador/notes-jugador/crud/update-notes-jugador/update-notes-jugador.component';
import { MainmenuComponent } from './Mainmenu/mainmenu/mainmenu.component';
import { MenuprincipalMasterComponent } from './master/menuprincipal-master/menuprincipal-master.component';
import { MonsterDiceComponent } from './master/monster-dice/monster-dice.component';
import { CreateMonstersComponent } from './master/monsters/crud/create-monsters/create-monsters.component';
import { ListMonstersComponent } from './master/monsters/crud/list-monsters/list-monsters.component';
import { UpdateMonstersComponent } from './master/monsters/crud/update-monsters/update-monsters.component';
import { CreateNotesComponent } from './master/notes/crud/create-notes/create-notes.component';
import { ListNotesComponent } from './master/notes/crud/list-notes/list-notes.component';
import { UpdateNotesComponent } from './master/notes/crud/update-notes/update-notes.component';
import { HomeComponent } from './principal/home/home.component';
import { LoginComponent } from './principal/login/login.component';
import { RegisterComponent } from './principal/register/register.component';

const routes: Routes = [
  {path: "createmonsters", component:CreateMonstersComponent},
  {path: "listmonsters", component:ListMonstersComponent},
  { path: 'updatemonsters/:id', component: UpdateMonstersComponent },
  {path: "listnotesMaster", component:ListNotesComponent},
  { path: 'updatenotesMaster/:id', component: UpdateNotesComponent },
  { path: 'createnotesMaster', component: CreateNotesComponent },
  { path: 'createnotesJugador', component: CreateNotesJugadorComponent },
  { path: 'listnotesJugador', component: ListNotesJugadorComponent },
  { path: 'updatenotesJugador/:id', component: UpdateNotesJugadorComponent },
  {path: 'mainmenu', component:MainmenuComponent},
  {path: 'character-dice', component:CharacterDiceComponent},
  {path: 'menuprincipal-player', component:MenuprincipalPlayerComponent},
  {path: 'character-sheet', component:UpdateCharacterComponent},
  {path: 'menuprincipal-master', component:MenuprincipalMasterComponent},
  {path: 'monster-dice', component:MonsterDiceComponent},
  {path: 'home', component:HomeComponent},
  {path: 'login', component:LoginComponent},
  {path: 'register', component:RegisterComponent},
  {path: 'character-creation', component:CharacterCreationComponent},
  {path: 'character-information', component:CharacterInformationComponent},
  {path: 'character-viewer', component:CharacterViewerComponent},
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
