import { Component, OnInit } from '@angular/core';
import * as $ from "jquery";

@Component({
  selector: 'app-character-dice',
  templateUrl: './character-dice.component.html',
  styleUrls: ['./character-dice.component.css']
})
export class CharacterDiceComponent implements OnInit {

  constructor() { }

  ngOnInit(): void {
  }

  dice1d4() {
    $('.number1d4').text(" ");
    $('.number1d4').text(Math.floor(Math.random() * 4) + 1);
  }

  dice1d6() {
    $('.number1d6').text(" ");
    $('.number1d6').text(Math.floor(Math.random() * 6) + 1);
  }

  dice1d8() {
    $('.number1d8').text(" ");
    $('.number1d8').text(Math.floor(Math.random() * 8) + 1);
  }

  dice1d12() {
    $('.number1d12').text(" ");
    $('.number1d12').text(Math.floor(Math.random() * 12) + 1);
  }

  dice1d20() {
    $('.number1d20').text(" ");
    $('.number1d20').text(Math.floor(Math.random() * 20) + 1);
  }

  dice1d100() {
    $('.number1d100').text(" ");
    $('.number1d100').text(Math.floor(Math.random() * 100) + 1);
  }
}
