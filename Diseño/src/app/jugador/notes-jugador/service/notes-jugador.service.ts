import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class NotesJugadorService {

  private baseUrl = 'http://localhost:8181/api/notes'

  constructor(private http: HttpClient) { }

  getNotes(id:number) : Observable<any>{
    return this.http.get(`${this.baseUrl}/${id}`);
  }

  createNotes(notes: Object): Observable<Object> {
    return this.http.post(`${this.baseUrl}`, notes);
  }

  updateNotes(id: number, value: any): Observable<Object> {
    return this.http.put(`${this.baseUrl}/${id}`, value);
  }

  deleteNotes(id: number): Observable<any> {
    return this.http.delete(`${this.baseUrl}/${id}`, { responseType: 'text' });
  }

  getNotesList(): Observable<any> {
    return this.http.get(`${this.baseUrl}`);
  }
}
