import { TestBed } from '@angular/core/testing';

import { NotesJugadorService } from './notes-jugador.service';

describe('NotesJugadorService', () => {
  let service: NotesJugadorService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(NotesJugadorService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
