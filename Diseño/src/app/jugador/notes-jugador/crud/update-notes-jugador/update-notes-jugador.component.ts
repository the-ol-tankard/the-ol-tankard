import { Component, OnInit } from '@angular/core';
import swal from 'sweetalert2';
import { ActivatedRoute, Router } from '@angular/router';
import { notesJugador } from '../../model/notes-jugador';
import { NotesJugadorService } from '../../service/notes-jugador.service';

@Component({
  selector: 'app-update-notes-jugador',
  templateUrl: './update-notes-jugador.component.html',
  styleUrls: ['./update-notes-jugador.component.css']
})
export class UpdateNotesJugadorComponent implements OnInit {

  id:number;

  notesJugador: notesJugador;

  constructor(private route: ActivatedRoute, private router: Router,
    private NotesJugadorService: NotesJugadorService) { }

    ngOnInit(){
      this.notesJugador = new notesJugador();

      this.id = this.route.snapshot.params['id'];

      this.NotesJugadorService.getNotes(this.id)
      .subscribe(data => {
        console.log(data)
        this.notesJugador = data;
        }, error => console.log(error));
    }
    updateNotes(){
      this.NotesJugadorService.updateNotes(this.id, this.notesJugador)
      .subscribe(data => {
        console.log(data);
        this.notesJugador = new notesJugador();
        this.gotoList();
      }, error => console.log(error));
    }

    onSubmit(){
      this.updateNotes();
    }

    gotoList(){
      this.router.navigate(['/listnotesJugador'])
    }

    showMessage(){
      swal.fire({
        icon: 'success',
        title: 'Successfully updated',
        iconColor: '#d32f2f',
        confirmButtonColor: '#d32f2f'
      })
    }

}
