import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Observable } from 'rxjs';
import { notesJugador } from '../../model/notes-jugador';
import { NotesJugadorService } from '../../service/notes-jugador.service';
import swal from 'sweetalert2';

@Component({
  selector: 'app-list-notes-jugador',
  templateUrl: './list-notes-jugador.component.html',
  styleUrls: ['./list-notes-jugador.component.css']
})
export class ListNotesJugadorComponent implements OnInit {

  notesJugador: Observable<notesJugador[]>;

  constructor(private NotesJugadorService: NotesJugadorService,
    private router: Router) { }

  ngOnInit() {
      this.reloadData();
  }

  reloadData() {
    this.notesJugador = this.NotesJugadorService.getNotesList();
 }

 deleteNotes(id: number) {
   this.NotesJugadorService.deleteNotes(id)
   .subscribe(
     data => {
       console.log(data);
       this.reloadData();
     },
     error => console.log(error));
 }

 updateNotes(id:number) {
   this.router.navigate(['updatenotesJugador', id]);
 }

 createNotes(){
   this.router.navigate(['createnotesJugador'])
 }

 confirmacionDelete(id:number){
  swal.fire({
    title: 'Do you want to delete the note?',
    showDenyButton: true,
    confirmButtonText: 'Delete',
    denyButtonText:'Cancel',
    confirmButtonColor: '#d32f2f',
    denyButtonColor: '#d32f2f',
  }).then((result) => {
    if (result.isConfirmed) {
      this.deleteNotes(id);
      swal.fire({
        icon: 'success',
        title: 'Successfully deleted',
        iconColor: '#d32f2f',
        confirmButtonColor: '#d32f2f'
      })
    } else if (result.isDenied) {

    }
  })
 }

 gotoList(){
  this.router.navigate(['/listnotesJugador'])
}

 stoppropagation(){
   event.stopPropagation();
 }


}
