import { Component, OnInit } from '@angular/core';
import Swal from 'sweetalert2';

@Component({
  selector: 'app-character-information',
  templateUrl: './character-information.component.html',
  styleUrls: ['./character-information.component.css']
})
export class CharacterInformationComponent implements OnInit {

  constructor() { }

  ngOnInit(): void {
  }

  successAlert(){

    Swal.fire({
      icon: 'success',
      title: 'Success!',
      text: 'Your character has been created!',
      iconColor: '#ef5350',
      confirmButtonColor: '#d32f2f',
      confirmButtonText: '<a href="/character-viewer" style="text-decoration:none; color:#fff;">OK</a>',
    })

  }

}
