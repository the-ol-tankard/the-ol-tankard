import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-character-creation',
  templateUrl: './character-creation.component.html',
  styleUrls: ['./character-creation.component.css']
})
export class CharacterCreationComponent implements OnInit {

  constructor() { }

  ngOnInit(): void {
  }

  activeRace(id: number) {
    $('.race-banner').removeClass('active')
    $('#race-' + id).toggleClass('active')
    if (id == 1) {
      $('#race-name').text('Dragonborn')
      $('#race-description').text('Born of dragons, as their name proclaims, the dragonborn walk proudly through a world that greets them with fearful incomprehension. Shaped by draconic gods or the dragons themselves, dragonborn originally hatched from dragon eggs as a unique race, combining the best attributes of dragons and humanoids.')
      $('#race-render').attr('src', '../../assets/img/dragonborn_render.png');
    }
    if (id == 2) {
      $('#race-name').text('Dwarf')
      $('#race-description').text('Kingdoms rich in ancient grandeur, halls carved into the roots of mountains, the echoing of picks and hammers in deep mines and blazing forges, a commitment to clan and tradition, and a burning hatred of goblins and orcs—these common threads unite all dwarves.')
      $('#race-render').attr('src', '../../assets/img/dwarf_render.png');
    }
    if (id == 3) {
      $('#race-name').text('Elf')
      $('#race-description').text('Elves are a magical people of otherworldly grace, living in the world but not entirely part of it. They live in places of ethereal beauty, in the midst of ancient forests or in silvery spires glittering with faerie light, where soft music drifts through the air and gentle fragrances waft on the breeze.')
      $('#race-render').attr('src', '../../assets/img/elf_render.png');
    }
    if (id == 4) {
      $('#race-name').text('Gnome')
      $('#race-description').text('A constant hum of busy activity pervades the warrens and neighborhoods where gnomes form their close-knit communities. Louder sounds punctuate the hum: a crunch of grinding gears here, a minor explosion there, a yelp of surprise or triumph, and especially bursts of laughter.')
      $('#race-render').attr('src', '../../assets/img/gnome_render.png');
    }
    if (id == 5) {
      $('#race-name').text('Half-Elf')
      $('#race-description').text('Walking in two worlds but truly belonging to neither, half-elves combine what some say are the best qualities of their elf and human parents: human curiosity, inventiveness, and ambition tempered by the refined senses, love of nature, and artistic tastes of the elves.')
      $('#race-render').attr('src', '../../assets/img/halfelf_render.png');
    }
    if (id == 6) {
      $('#race-name').text('Halfling')
      $('#race-description').text('The comforts of home are the goals of most halflings’ lives: a place to settle in peace and quiet, far from marauding monsters and clashing armies; a blazing fire and a generous meal; fine drink and fine conversation. ')
      $('#race-render').attr('src', '../../assets/img/halfling_render.png');
    }
    if (id == 7) {
      $('#race-name').text('Half-Orc')
      $('#race-description').text('Whether united under the leadership of a mighty warlock or having fought to a standstill after years of conflict, orc and human tribes sometimes form alliances, joining forces into a larger horde to the terror of civilized lands nearby. When these alliances are sealed by marriages, half-orcs are born.')
      $('#race-render').attr('src', '../../assets/img/halforc_render.png');
    }
    if (id == 8) {
      $('#race-name').text('Human')
      $('#race-description').text('In the reckonings of most worlds, humans are the youngest of the common races, late to arrive on the world scene and short-lived in comparison to dwarves, elves, and dragons. Perhaps it is because of their shorter lives that they strive to achieve as much as they can in the years they are given.')
      $('#race-render').attr('src', '../../assets/img/human_render.png');
    }
    if (id == 9) {
      $('#race-name').text('Tiefling')
      $('#race-description').text('To be greeted with stares and whispers, to suffer violence and insult on the street, to see mistrust and fear in every eye: this is the lot of the tiefling. And to twist the knife, tieflings know that this is because a pact struck generations ago infused the essence of Asmodeus—overlord of the Nine Hells—into their bloodline.')
      $('#race-render').attr('src', '../../assets/img/tiefling_render.png');
    }
  }

  activeClass(id: number) {
    $('.class-icon').removeClass('active')
    $('#class-' + id).toggleClass('active')
    if (id == 1) {
      $('#class-name').text('Barbarian')
      $('#class-description').text('These barbarians, different as they might be, are defined by their rage: unbridled, unquenchable, and unthinking fury. More than a mere emotion, their anger is the ferocity of a cornered predator, the unrelenting assault of a storm, the churning turmoil of the sea.')
      $('#class-render').attr('src', '../../assets/img/barbarian_render.png');
    }
    if (id == 2) {
      $('#class-name').text('Bard')
      $('#class-description').text('Whether scholar, skald, or scoundrel, a bard weaves magic through words and music to inspire allies, demoralize foes, manipulate minds, create illusions, and even heal wounds.')
      $('#class-render').attr('src', '../../assets/img/bard_render.png');
    }
    if (id == 3) {
      $('#class-name').text('Cleric')
      $('#class-description').text('Clerics are intermediaries between the mortal world and the distant planes of the gods. As varied as the gods they serve, clerics strive to embody the handiwork of their deities. No ordinary priest, a cleric is imbued with divine magic.')
      $('#class-render').attr('src', '../../assets/img/cleric_render.png');
    }
    if (id == 4) {
      $('#class-name').text('Druid')
      $('#class-description').text('Whether calling on the elemental forces of nature or emulating the creatures of the animal world, druids are an embodiment of nature’s resilience, cunning, and fury. They claim no mastery over nature. Instead, they see themselves as extensions of nature’s indomitable will.')
      $('#class-render').attr('src', '../../assets/img/druid_render.png');
    }
    if (id == 5) {
      $('#class-name').text('Fighter')
      $('#class-description').text('Questing knights, conquering overlords, royal champions, elite foot soldiers, hardened mercenaries, and bandit kings—as fighters, they all share an unparalleled mastery with weapons and armor, and a thorough knowledge of the skills of combat. And they are well acquainted with death, both meting it out and staring it defiantly in the face.')
      $('#class-render').attr('src', '../../assets/img/fighter_render.png');
    }
    if (id == 6) {
      $('#class-name').text('Monk')
      $('#class-description').text('Whatever their discipline, monks are united in their ability to magically harness the energy that flows in their bodies. Whether channeled as a striking display of combat prowess or a subtler focus of defensive ability and speed, this energy infuses all that a monk does.')
      $('#class-render').attr('src', '../../assets/img/monk_render.png');
    }
    if (id == 7) {
      $('#class-name').text('Paladin')
      $('#class-description').text('Whatever their origin and their mission, paladins are united by their oaths to stand against the forces of evil. Whether sworn before a god’s altar and the witness of a priest, in a sacred glade before nature spirits and fey beings, or in a moment of desperation and grief with the dead as the only witness, a paladin’s oath is a powerful bond. It is a source of power that turns a devout warrior into a blessed champion.')
      $('#class-render').attr('src', '../../assets/img/paladin_render.png');
    }
    if (id == 8) {
      $('#class-name').text('Ranger')
      $('#class-description').text('Far from the bustle of cities and towns, past the hedges that shelter the most distant farms from the terrors of the wild, amid the dense-packed trees of trackless forests and across wide and empty plains, rangers keep their unending watch.')
      $('#class-render').attr('src', '../../assets/img/ranger_render.png');
    }
    if (id == 9) {
      $('#class-name').text('Rogue')
      $('#class-description').text('Rogues rely on skill, stealth, and their foes’ vulnerabilities to get the upper hand in any situation. They have a knack for finding the solution to just about any problem, demonstrating a resourcefulness and versatility that is the cornerstone of any successful adventuring party.')
      $('#class-render').attr('src', '../../assets/img/rogue_render.png');
    }
    if (id == 10) {
      $('#class-name').text('Sorcerer')
      $('#class-description').text('Sorcerers carry a magical birthright conferred upon them by an exotic bloodline, some otherworldly influence, or exposure to unknown cosmic forces. One can’t study sorcery as one learns a language, any more than one can learn to live a legendary life. No one chooses sorcery; the power chooses the sorcerer.')
      $('#class-render').attr('src', '../../assets/img/sorcerer_render.png');
    }
    if (id == 11) {
      $('#class-name').text('Warlock')
      $('#class-description').text('Warlocks are seekers of the knowledge that lies hidden in the fabric of the multiverse. Through pacts made with mysterious beings of supernatural power, warlocks unlock magical effects both subtle and spectacular. Drawing on the ancient knowledge of beings such as fey nobles, demons, devils, hags, and alien entities of the Far Realm, warlocks piece together arcane secrets to bolster their own power.')
      $('#class-render').attr('src', '../../assets/img/warlock_render.png');
    }
    if (id == 12) {
      $('#class-name').text('Wizard')
      $('#class-description').text('Wizards are supreme magic-users, defined and united as a class by the spells they cast. Drawing on the subtle weave of magic that permeates the cosmos, wizards cast spells of explosive fire, arcing lightning, subtle deception, and brute-force mind control. Their magic conjures monsters from other planes of existence, glimpses the future, or turns slain foes into zombies. Their mightiest spells change one substance into another, call meteors down from the sky, or open portals to other worlds.')
      $('#class-render').attr('src', '../../assets/img/wizard_render.png');
    }
  }
}
