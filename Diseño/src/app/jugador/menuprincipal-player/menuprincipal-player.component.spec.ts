import { ComponentFixture, TestBed } from '@angular/core/testing';

import { MenuprincipalPlayerComponent } from './menuprincipal-player.component';

describe('MenuprincipalPlayerComponent', () => {
  let component: MenuprincipalPlayerComponent;
  let fixture: ComponentFixture<MenuprincipalPlayerComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ MenuprincipalPlayerComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(MenuprincipalPlayerComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
