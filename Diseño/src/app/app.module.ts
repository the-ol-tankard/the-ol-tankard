import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { CreateMonstersComponent } from './master/monsters/crud/create-monsters/create-monsters.component';
import { FormsModule } from '@angular/forms';
import { HttpClient, HttpClientModule } from '@angular/common/http';
import { ListMonstersComponent } from './master/monsters/crud/list-monsters/list-monsters.component';
import { UpdateMonstersComponent } from './master/monsters/crud/update-monsters/update-monsters.component';
import { ListNotesComponent } from './master/notes/crud/list-notes/list-notes.component';
import { UpdateNotesComponent } from './master/notes/crud/update-notes/update-notes.component';
import { CreateNotesComponent } from './master/notes/crud/create-notes/create-notes.component';
import { CreateNotesJugadorComponent } from './jugador/notes-jugador/crud/create-notes-jugador/create-notes-jugador.component';
import { ListNotesJugadorComponent } from './jugador/notes-jugador/crud/list-notes-jugador/list-notes-jugador.component';
import { UpdateNotesJugadorComponent } from './jugador/notes-jugador/crud/update-notes-jugador/update-notes-jugador.component';
import { MainmenuComponent } from './Mainmenu/mainmenu/mainmenu.component';
import { CharacterDiceComponent } from './jugador/character-dice/character-dice.component';
import { UpdateCharacterComponent } from './jugador/jugador/crud/update-character/update-character.component';
import { MenuprincipalPlayerComponent } from './jugador/menuprincipal-player/menuprincipal-player.component';
import { MonsterDiceComponent } from './master/monster-dice/monster-dice.component';
import { MenuprincipalMasterComponent } from './master/menuprincipal-master/menuprincipal-master.component';
import { HomeComponent } from './principal/home/home.component';
import { LoginComponent } from './principal/login/login.component';
import { RegisterComponent } from './principal/register/register.component';
import { CharacterCreationComponent } from './jugador/jugador/crud/character-creation/character-creation.component';
import { CharacterViewerComponent } from './jugador/jugador/crud/character-viewer/character-viewer.component';

@NgModule({
  declarations: [
    AppComponent,
    CreateMonstersComponent,
    ListMonstersComponent,
    UpdateMonstersComponent,
    ListNotesComponent,
    UpdateNotesComponent,
    CreateNotesComponent,
    CreateNotesJugadorComponent,
    ListNotesJugadorComponent,
    UpdateNotesJugadorComponent,
    MainmenuComponent,
    CharacterDiceComponent,
    UpdateCharacterComponent,
    MenuprincipalPlayerComponent,
    MonsterDiceComponent,
    MenuprincipalMasterComponent,
    HomeComponent,
    LoginComponent,
    RegisterComponent,
    CharacterCreationComponent,
    CharacterViewerComponent,
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    FormsModule,
    HttpClientModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
