export class monsters{
  id: number;
  name: String;
  description: string;
  armorClass: number;
  hitPoints: number;
  attack: string;
  speed: number;
  str: number;
  dex: number;
  con: number;
  inte: number;
  wis: number;
  cha: number;
}
