import { Component, OnInit } from '@angular/core';
import { monsters } from '../../model/monsters';
import { ActivatedRoute, Router } from '@angular/router';
import { MonstersService} from '../../service/monsters.service';
import { error } from 'protractor';
import swal from 'sweetalert2';

@Component({
  selector: 'app-update-monsters',
  templateUrl: './update-monsters.component.html',
  styleUrls: ['./update-monsters.component.css']
})
export class UpdateMonstersComponent implements OnInit {

  id: number;
  monsters: monsters;


  constructor(private route: ActivatedRoute, private router: Router,
    private MonstersService: MonstersService) { }

  ngOnInit() {

    this.monsters = new monsters();

    this.id = this.route.snapshot.params['id'];

    this.MonstersService.getMonster(this.id)
    .subscribe(data => {
      console.log(data)
      this.monsters = data;
      }, error => console.log(error));
  }

  updateMonsters(){
    this.MonstersService.updateMonster(this.id, this.monsters)
    .subscribe(data => {
      console.log(data);
      this.monsters = new monsters();
      this.gotoList();
    }, error => console.log(error));
  }

  onSubmit(){
    this.updateMonsters();
  }

  gotoList(){
    this.router.navigate(['/listmonsters'])
  }

  showMessage(){
    swal.fire({
      icon: 'success',
      title: 'Successfully updated',
      iconColor: '#d32f2f',
      confirmButtonColor: '#d32f2f'
    })
  }
}


