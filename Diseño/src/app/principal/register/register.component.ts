import { Component, OnInit } from '@angular/core';
import Swal from 'sweetalert2';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.css']
})
export class RegisterComponent implements OnInit {

  constructor() { }

  ngOnInit(): void {
  }

  successAlert(){

    Swal.fire({
      icon: 'success',
      title: 'Success!',
      text: 'Your account has been created!',
      iconColor: '#ef5350',
      confirmButtonColor: '#d32f2f',
      confirmButtonText: '<a href="/character-viewer" style="text-decoration:none; color:#fff;">OK</a>',
    })

  }

}
