import { Component, OnInit } from '@angular/core';
import swal from 'sweetalert2';
import * as $ from "jquery";

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  constructor() { }

  ngOnInit(): void {
  }

  successAlert(){

    swal.fire({
      icon: 'success',
      title: 'Success!',
      text: 'You have successfully logged in!',
      iconColor: '#ef5350',
      confirmButtonColor: '#d32f2f',
      confirmButtonText: '<a href="/character-viewer" style="text-decoration:none; color:#fff;">OK</a>',
    })

  }

}
